{
  description = "Vasara Tryton Application";

  nixConfig = {
    extra-trusted-public-keys =
      "vasara-bpm.cachix.org-1:T18iQZQvYDy/6VdGmttnkkq7rYi3JP0S1RjjdnXNu/4=";
    extra-substituters = "https://vasara-bpm.cachix.org";
  };

  inputs = {

    # Generic
    flake-utils.url = "github:numtide/flake-utils";
    nixpkgs.url = "github:NixOS/nixpkgs/release-23.11";
    nixpkgs-unstable.url = "github:NixOS/nixpkgs/master";
    flake-compat = {
      url = "github:edolstra/flake-compat";
      flake = false;
    };
    gitignore = {
      url = "github:hercules-ci/gitignore.nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    poetry2nix = {
      url = "github:nix-community/poetry2nix";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.flake-utils.follows = "flake-utils";
    };
    bower2nix = {
      url = "github:rvl/bower2nix";
      flake = false;
    };

    # Vasara
    vasara-bpm = {
      url = "gitlab:vasara-bpm/pkgs";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.flake-utils.follows = "flake-utils";
    };

  };

  outputs = { self, nixpkgs, nixpkgs-unstable, flake-utils, ... }@inputs:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs-unstable = import nixpkgs-unstable {
          inherit system;
          overlays = [
            (self: super: {
              poetry = super.poetry.override { python3 = self.python311; };
            })
          ];
        };
        pkgs = import nixpkgs {
          inherit system;
          overlays = [
            inputs.poetry2nix.overlays.default
            (self: super: { inherit (inputs.gitignore.lib) gitignoreSource; })
          ];
        };
        overrides = pkgs.poetry2nix.overrides.withDefaults (self: super: { });
        python =
          pkgs.python311.override { packageOverrides = self: super: { }; };
        call-name = "vasara-tryton";

      in {
        # Application
        apps.default = {
          type = "app";
          program = self.packages.${system}.default + "/bin/trytond";
        };

        # Static
        packages.static = pkgs.stdenv.mkDerivation rec {
          name = "vasara-tryton-sao";
          version = "7.0.1";
          src = pkgs.fetchurl {
            url =
              "https://registry.npmjs.org/tryton-sao/-/tryton-sao-${version}.tgz";
            hash =
              "sha256-xsjmTREEy1Dg0uGxiSCcDABQTU9IANGbYbAAjeDgCwM=";
          };
          buildPhase = "";
          installPhase = let
            bower_json = pkgs.stdenv.mkDerivation {
              name = "bower.json";
              inherit src;
              buildPhase = "";
              installPhase = ''
                mkdir -p $out
                cp -a bower.json $out
              '';
            };
            bower_components = pkgs.buildBowerComponents {
              name = "tryton-sao-bower-components";
              generated = ./bower.nix;
              src = bower_json;
            };
          in ''
            mkdir $out
            cp index.html $out
            cp -a dist $out
            cp -a bower.json $out
            cp -a images $out
            cp -a locale $out
            cp -a ${bower_components}/bower_components $out/bower_components
            touch $out/custom.js
            touch $out/custom.css
            echo '{}' > $out/locale/en.json
            echo '{}' > $out/locale/en_US.json
          '';
        };

        # Python
        packages.env = (pkgs.poetry2nix.mkPoetryEnv {
          projectDir = pkgs.gitignoreSource ./.;
          preferWheels = true;
          inherit overrides python;
        }).withPackages
        (ps: with ps; [
          psycopg2
          trytond
          trytond-party
          trytond-authentication-saml
        ]);

        packages.devenv = (pkgs.poetry2nix.mkPoetryEnv {
          projectDir = pkgs.gitignoreSource ./.;
          preferWheels = true;
          inherit overrides python;
        }).withPackages (ps:
          with ps; [
            psycopg2
            trytond
            trytond-party
            trytond-authentication-saml
            black
            mypy
            pytest
            pytest-cov
            isort
            coverage
          ]);

        # Default package
        packages.default = pkgs.stdenv.mkDerivation {
          name = "${call-name}";
          src = pkgs.gitignoreSource ./.;
          propagatedBuildInputs = with pkgs; [ nginx ];
          builder = pkgs.writeScript "builder.sh" ''
            source $stdenv/setup;
            mkdir -p $out/bin/
            cat > $out/bin/${call-name} << EOF
            #!${pkgs.bash}/bin/sh

            # Exit on error
            set -e

            # Copy output to new location
            local=\$(pwd)
            if [[ -d \$local/public ]]; then chmod u+w -R \$local/public; fi
            if [[ -f \$local/nginx.conf ]]; then chmod u+w  \$local/nginx.conf; fi
            cp -R ${self.packages.${system}.static} \$local/public
            chmod u+w -R \$local/public

            cat > \$local/uwsgi.ini << END_OF_FILE
            [uwsgi]
            module = trytond.application:app
            master = true
            processes = 4
            socket = \$UWSGI_HOST:\$UWSGI_PORT
            plugins = python3
            END_OF_FILE

            mkdir -p \$local/nginx/logs
            cat > \$local/nginx.conf << END_OF_FILE
            daemon off;
            worker_processes 4;
            error_log /dev/stdout info;

            events {
              use epoll;
              worker_connections 128;
            }

            http {
              include ${pkgs.nginx}/conf/mime.types;
              default_type application/octet-stream;
              sendfile on;
              access_log /dev/stdout;
              server {
                listen \$HTTP_PORT;
                server_name \$HTTP_HOST;
                gzip on;
                gzip_types text/css application/javascript;
                gzip_min_length 1000;
                location / {
                  root \$local/public;
                  index index.html;
                  # No-Proxy
                  add_header Last-Modified \\\$date_gmt;
                  add_header Cache-Control 'no-store, no-cache, must-revalidate, proxy-revalidate, max-age=0';
                  if_modified_since off;
                  expires off;
                  etag off;
                  # CSP
                  add_header X-Frame-Options "deny" always;
                  add_header X-XSS-Protection "mode=block" always;
                  add_header X-Content-Type-Options "nosniff" always;
                  add_header Referrer-Policy "same-origin" always;
                  # add_header Content-Security-Policy "default-src 'self'; style-src 'self' 'unsafe-inline'; object-src 'none'" always;
                  # add_header Content-Security-Policy "default-src 'self' blob:; script-src 'self' 'unsafe-inline'; style-src 'self' 'unsafe-inline'; object-src 'none'" always;
                  # add_header Feature-Policy "accelerometer 'none'; ambient-light-sensor 'none'; autoplay 'none'; battery 'none'; camera 'none'; display-capture 'none'; document-domain 'none'; encrypted-media 'none'; execution-while-not-rendered 'none'; execution-while-out-of-viewport 'none'; fullscreen 'none'; geolocation 'none'; gyroscope 'none'; layout-animations 'none'; legacy-image-formats 'none'; magnetometer 'none'; microphone 'none'; midi 'none'; navigation-override 'none'; oversized-images 'none'; payment 'none'; picture-in-picture 'none'; publickey-credentials 'none'; sync-xhr 'none'; usb 'none'; vr 'none'; wake-lock 'none'; xr-spatial-tracking 'none'" always;
                  #
                  try_files \\\$uri @uwsgi;
                }
                location @uwsgi {
                  include ${pkgs.nginx}/conf/uwsgi_params;
                  uwsgi_pass \$UWSGI_HOST:\$UWSGI_PORT;
                }
              }
            }
            END_OF_FILE

            exec nginx -p \$local/nginx -c \$local/nginx.conf
            EOF
            chmod u+x $out/bin/${call-name}
          '';
        };

        packages.image = pkgs.dockerTools.streamLayeredImage {
          name = "vasara-bpm/vasara-tryton/${call-name}";
          tag = "latest";
          created = "now";
          contents = [
            (pkgs.buildEnv {
              name = "image-contents";
              paths = [
                pkgs.busybox
                pkgs.dockerTools.fakeNss
                pkgs.dockerTools.usrBinEnv
                pkgs.tini
                pkgs.nginx
                pkgs.xmlsec
                (pkgs.uwsgi.override { plugins = [ "python3" ]; })
                self.packages.${system}.env
                self.packages.${system}.default
              ];
              pathsToLink = [ "/etc" "/sbin" "/bin" ];
            })
          ];
          extraCommands = ''
            mkdir -p usr/bin && ln -s /sbin/env usr/bin/env
            mkdir -p tmp && chmod a+rxwt tmp
            mkdir -p var/log/nginx var/cache/nginx && chmod a+rxwt -R var
            mkdir -p app && chmod a+rxwt app
          '';
          config = {
            WorkingDir = "/app";
            Entrypoint = [
              "${pkgs.tini}/bin/tini"
              "--"
              "${self.packages.${system}.default}/bin/${call-name}"
            ];
            Env = [
              "TMPDIR=/tmp"
              "HOME=/tmp"
              "HTTP_HOST=0.0.0.0"
              "HTTP_PORT=8080"
              "UWSGI_HOST=0.0.0.0"
              "UWSGI_PORT=8000"
              "PYTHONPATH=${self.packages.${system}.env}/${
                self.packages.${system}.env.sitePackages
              }"
            ];
            Labels = { };
            User = "nobody";
          };
        };

        # Development shell
        devShells.default = pkgs.mkShell {
          buildInputs = [
            pkgs.entr
            pkgs.gnumake
            pkgs.jq
            pkgs.nginx
            pkgs.xmlsec
            pkgs-unstable.poetry
            self.packages.${system}.devenv
            (pkgs.uwsgi.override { plugins = [ "python3" ]; })
          ];
          shellHook = ''
            export PYTHONPATH=$(pwd)/src:$PYTHONPATH
          '';
        };

        devShells.bower2nix = let
          bower2nix_patch = pkgs.fetchurl {
            url =
              "https://patch-diff.githubusercontent.com/raw/rvl/bower2nix/pull/23.patch";
            sha256 = "sha256-Rc44e0JXicqDuvcGzXrch3SdI5+O8TNTBZcTum2apGs=";
          };
          bower2nix_src = pkgs.stdenv.mkDerivation {
            name = "bower2nix-src";
            src = inputs.bower2nix;
            patches = [ bower2nix_patch ];
            unpackPhase = "";
            buildPhase = "";
            installPhase = ''
              cp -a . $out
            '';
          };
        in pkgs.mkShell {
          buildInputs = [
            ((pkgs.callPackage bower2nix_src {
              inherit pkgs;
            }).package.overrideDerivation (old: {
              installPhase = old.installPhase + ''
                tsc
                cp -a dist $out
              '';
            }))
          ];
          shellHook = ''
            export PYTHONPATH=$(pwd)/src:$PYTHONPATH
          '';
        };

        devShells.with-podman =
          inputs.vasara-bpm.devShells.${system}.podman.overrideAttrs (old: {
            buildInputs = old.buildInputs
              ++ self.devShells.${system}.default.buildInputs;
          });

        formatter = pkgs.nixfmt;
      });
}
